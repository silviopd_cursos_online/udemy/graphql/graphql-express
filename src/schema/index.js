import { makeExecutableSchema } from "graphql-tools";
import "graphql-import-node";
import typeDefs from "./schema.graphql";
import { resolvers } from "../resolves";

export default makeExecutableSchema({
  typeDefs,
  resolvers
});
