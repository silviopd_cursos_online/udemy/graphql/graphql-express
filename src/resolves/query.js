export const query = {
  Query: {
    hello() {
      return `hola mundo`;
    },
    helloName(root, args) {
      return `hola ${args.name}`;
    }
  }
};
