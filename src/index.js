import express from "express";
import compression from "compression";
import cors from "cors";
import { ApolloServer } from "apollo-server-express";

import schema from "./schema";
import { createServer } from "http";

const app = express();

app.use("*", cors());

app.use(compression());

const server = new ApolloServer({
  schema,
  introspection: true
});

server.applyMiddleware({ app });

const httpServer = createServer(app);

app.use("/", (req, res) => {
  res.send("Bienvenido al curso de graphql");
});

httpServer.listen(3000, () => {
  console.log(`PORT 3000`);
});
